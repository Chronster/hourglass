#pragma once

#include "hourglass.h"
#include "stdafx.h"


Board::Board(const int width, const int height, const int verticalSpacing) {
	Board::width = width;
	Board::height = height;
	Board::boardPtr = &board1;
	Board::nextBoardPtr = &board2;

	Board::black = static_cast<char>(0);
	Board::yellow = static_cast<char>(1);
	Board::blue = static_cast<char>(2);

	Board::frameCounter = 0;

	p = 50;											//50% probability to get stuck if two sand grains are next to each other
	rng.seed(time(NULL));
	dis = std::uniform_int_distribution<>(0,100);
	//dis.min = 0;
	//dis.max = 1;

	Board::board1.resize(height);
	Board::board2.resize(height);

	int bluePixelPerRowCount = 1;

	for (int row=0; row<height/2; row++) {
		Board::board1[row].resize(width, blue);
		Board::board2[row].resize(width, blue);
		if (row%verticalSpacing == 0 && row!=0) {
			bluePixelPerRowCount++;
		}
		for (int column=bluePixelPerRowCount; column<width-bluePixelPerRowCount; column++) {
			Board::board1[row][column] = yellow;
			Board::board2[row][column] = yellow;
		}
	}

	for (int row=height/2; row<height; row++) {
		Board::board1[row].resize(width, blue);
		Board::board2[row].resize(width, blue);
		if (row%verticalSpacing == 0) {
			bluePixelPerRowCount--;
		}
		for (int column=bluePixelPerRowCount; column<width-bluePixelPerRowCount; column++) {
			Board::board1[row][column] = black;
			Board::board2[row][column] = black;
		}
	}
}

Board::~Board() {
}

void Board::updatePhysics() {
	int code, tmpRow, tmpColumn, boardHeight, boardWidth;
	if (Board::frameCounter % 2 == 0) {
		tmpRow=0;
		tmpColumn=0;
		boardHeight = height;
		boardWidth = width;
	} else {
		tmpRow=1;
		tmpColumn=1;
		boardHeight = height-1;
		boardWidth = width-1;
	}

	for (int row=tmpRow; row<boardHeight; row+=2) {
		for (int column=tmpColumn; column<boardWidth; column+=2) {
	//for (int row=height-2; row>1; row--) {
		//for (int column=width-2; column>1; column--) {
			code = 0;
			
			if (Board::boardPtr->at(row+1).at(column) == blue) {
				continue;
			} else if (Board::boardPtr->at(row+1).at(column) == yellow) {
				code += 100;
			}

			if (Board::boardPtr->at(row+1).at(column+1) == blue) {
				continue;
			} else if (Board::boardPtr->at(row+1).at(column+1) == yellow) {
				code += 1000;
				if (code == 1100) {
					continue;
				}
			}

			if (Board::boardPtr->at(row).at(column) == blue) {
				continue;
			} else if (Board::boardPtr->at(row).at(column) == yellow) {
				code += 1;
			}

			if (Board::boardPtr->at(row).at(column+1) == blue) {
				continue;
			} else if (Board::boardPtr->at(row).at(column+1) == yellow) {
				code += 10;
			}

			switch(code) {
				case 1:
					Board::nextBoardPtr->at(row).at(column) = black;
					Board::nextBoardPtr->at(row).at(column+1) = black;
					Board::nextBoardPtr->at(row+1).at(column) = yellow;
					Board::nextBoardPtr->at(row+1).at(column+1) = black;
					break;

				case 10:
					Board::nextBoardPtr->at(row).at(column) = black;
					Board::nextBoardPtr->at(row).at(column+1) = black;
					Board::nextBoardPtr->at(row+1).at(column) = black;
					Board::nextBoardPtr->at(row+1).at(column+1) = yellow;
					break;

				case 101:
				case 1010:
				case 1001:
				case 110:
					Board::nextBoardPtr->at(row).at(column) = black;
					Board::nextBoardPtr->at(row).at(column+1) = black;
					Board::nextBoardPtr->at(row+1).at(column) = yellow;
					Board::nextBoardPtr->at(row+1).at(column+1) = yellow;
					break;

				case 1011:
					Board::nextBoardPtr->at(row).at(column) = black;
					Board::nextBoardPtr->at(row).at(column+1) = yellow;
					Board::nextBoardPtr->at(row+1).at(column) = yellow;
					Board::nextBoardPtr->at(row+1).at(column+1) = yellow;
					break;

				case 111:
					Board::nextBoardPtr->at(row).at(column) = yellow;
					Board::nextBoardPtr->at(row).at(column+1) = black;
					Board::nextBoardPtr->at(row+1).at(column) = yellow;
					Board::nextBoardPtr->at(row+1).at(column+1) = yellow;
					break;

				case 11:
					int randomNumber = dis(rng);
					if (randomNumber <= Board::p) {
						Board::nextBoardPtr->at(row).at(column) = yellow;
						Board::nextBoardPtr->at(row).at(column+1) = yellow;
						Board::nextBoardPtr->at(row+1).at(column) = black;
						Board::nextBoardPtr->at(row+1).at(column+1) = black;
					} else {
						Board::nextBoardPtr->at(row).at(column) = black;
						Board::nextBoardPtr->at(row).at(column+1) = black;
						Board::nextBoardPtr->at(row+1).at(column) = yellow;
						Board::nextBoardPtr->at(row+1).at(column+1) = yellow;
					}

					break;
			}
		}
	}

	if (Board::nextBoardPtr == &(Board::board1)) {
		Board::nextBoardPtr = &(Board::board2);
		Board::boardPtr = &(Board::board1);
		board2 = board1;
	} else {
		Board::nextBoardPtr = &(Board::board1);
		Board::boardPtr = &(Board::board2);
		board1 = board2;
	}
	Board::frameCounter++;
}

void Board::draw(clan::Canvas canvas) {
	clan::Colorf black = clan::Colorf(0.0f, 0.0f, 0.0f, 1.0f);
	clan::Colorf yellow = clan::Colorf(1.0f, 1.0f, 0.0f, 1.0f);
	clan::Colorf blue = clan::Colorf(0.0f, 0.0f, 1.0f, 1.0f);
	clan::Colorf *color;

	for (int row=0; row<height; row++) {
		for (int column=0; column<width; column++) {
			if (Board::boardPtr->at(row).at(column) == static_cast<char>(0)) {
				color = &black;
			} else if (Board::boardPtr->at(row).at(column) == static_cast<char>(1)) {
				color = &yellow;
			} else {
				color = &blue;
			}
			canvas.draw_point(static_cast<float>(column), static_cast<float>(row), *color);
		}
	}
}