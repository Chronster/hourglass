#include "stdafx.h"

//using namespace clan;

// Choose the target renderer
//#define USE_SOFTWARE_RENDERER
//#define USE_OPENGL_1
#define USE_OPENGL_2

#ifdef USE_SOFTWARE_RENDERER
#include <ClanLib/swrender.h>
#endif

#ifdef USE_OPENGL_1
#include <ClanLib/gl1.h>
#endif

#ifdef USE_OPENGL_2
#include <ClanLib/gl.h>
#endif

int main(const std::vector<std::string> &args)
{
	const int WIDTH = 250;
	const int HEIGHT = 470;
	const int VERTICAL_SPACING = 2;

	SetupCore setup_core;
	SetupDisplay setup_display;
	SetupGL setup_gl;

	DisplayWindow window("Hourglass", WIDTH+15, HEIGHT+40);
	Canvas canvas(window);
	InputDevice keyboard = window.get_ic().get_keyboard();
	//clan::Font font(canvas, "Tahoma", 30);	// The clan prefix is required on linux due to a namespace conflict

	Board *hourglass = new Board(WIDTH, HEIGHT, VERTICAL_SPACING);

	while (!keyboard.get_keycode(keycode_escape))
	{
		// Draw with the canvas:
		canvas.clear(Colorf::blue);
 
		//Draw::line(canvas, 0, 110, 640, 110, Colorf::yellow);
		//font.draw_text(canvas, 100, 100, "Hello Soundworld!", Colorf::lightseagreen);
		hourglass->draw(canvas);
		hourglass->updatePhysics();
 
		// Draw any remaining queued-up drawing commands on canvas:
		canvas.flush();
				
		// Present the frame buffer content to the user:
		window.flip();
 
		// Read messages from the windowing system message queue, if any are available:
		KeepAlive::process();
	}

	return 0;
}


// Create global application object:
// You MUST include this line or the application start-up will fail to locate your application object.
Application app(&main);
