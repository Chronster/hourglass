#pragma once

#include <vector>
#include <random>
#include <time.h>

class Board {
  public:
	Board(const int width, const int height, const int verticalSpacing);
	~Board();

	void updatePhysics();
	void draw(clan::Canvas canvas);

  private:
	int width, height;

	std::vector<std::vector<char>> *boardPtr;
	std::vector<std::vector<char>> *nextBoardPtr;
	std::vector<std::vector<char>> board1;
	std::vector<std::vector<char>> board2;

	char black;
	char yellow;
	char blue;

	//std::random_device rd;
	int p;
	int frameCounter;
    std::mt19937 rng;
	std::uniform_int_distribution<> dis;

};